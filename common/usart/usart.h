#include <stdint.h>
#ifndef __USART_H
#define __USART_H
#define EX_UART_NUM UART_NUM_0
#define PATTERN_CHR_NUM    (3)         /*!< Set the number of consecutive and identical characters received by receiver which defines a UART pattern*/
#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)

typedef void (*usart_handler_t)(uint8_t *data, uint16_t length);
void usart_init(void);
void usart_set_callback(void *cb);
void usart_puts(uint8_t *data, uint16_t length);
#endif