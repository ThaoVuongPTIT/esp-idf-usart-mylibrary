/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "usart.h"
#include "output.h"
#include "string.h"
#define BLINK_GPIO 2
void usart_sendData_CallBack(uint8_t *data, uint16_t length)
{
    if(strcmp((char*)data, "ON") == 0){
        usart_puts((uint8_t*)"LED ON\n", 6);
        output_set(2);
    }
    else if(strcmp((char*)data, "OFF") == 0){
        usart_puts((uint8_t *)"LED OFF\n", 7);
        output_clear(2);
    }
}
void app_main(void)
{
    output_create(2);
    usart_set_callback(usart_sendData_CallBack);
    usart_init();
}
